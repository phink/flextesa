Flextesa: Flexible Tezos Sandboxes
==================================

Build
-----

You need, Tezos' libraries (with `proto_alpha`) opam-installed or locally
vendored:

    make vendors

Then:

    make

